import { expect } from 'chai';

import { set, watch } from '../main';
import { put } from './array';

describe('watch(array, ...)', () => {
  context(`when @each token is used ( keyGraph = '@each.b' )`, () => {
    let watchCount = 0;
    const watcher = () => {
      watchCount++;
    };

    beforeEach(() => {
      watchCount = 0;
    });

    it('should trigger when setting a[0].b', () => {
      const a: { b: number }[] = [{ b: 0 }];
      watch(a, '@each.b', watcher);
      set(a[0], 'b', 1);
      expect(watchCount).to.equal(1);
    });
    it('should trigger when using put', () => {
      const a: { b: number }[] = [{ b: 0 }];
      watch(a, '@each.b', watcher);
      put(a, 0, { b: 1 });
      expect(watchCount).to.equal(1);
    });
  });
  context(`when numeric index key is used "0.b"`, () => {
    let watchCount = 0;
    const watcher = () => {
      watchCount++;
    };

    beforeEach(() => {
      watchCount = 0;
    });

    it('should not trigger when using put', () => {
      const a: { b: number }[] = [{ b: 0 }];
      watch(a, '0.b,', watcher);
      put(a, 0, { b: 0 });
      expect(watchCount).to.equal(0);
    });
  });
});