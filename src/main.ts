import { notifyChange, watch, watchKey } from './internals';
import { Key } from './key-graph/types';

export function set <
  T extends object,
  K extends keyof T & Key
> (
  obj: T, key: K, value: T[K]
): T[K] {
  const oldValue = obj[key];
  obj[key] = value;

  if (obj[key] !== oldValue) {
    notifyChange(obj, key);
  }

  return value;
}

export { watch, watchKey };
