import { Key, KeyBranch, KeyGraph, KeyLink } from './key-graph/types';
import { parseKeyGraph } from './key-graph/parse';

export type Terminator = () => void;
export interface TerminatorGraph extends
  Array<TerminatorGraph | Terminator | Set<TerminatorGraph>> {}

export type WatchCallback = (meta?: any) => void;

export interface Handler {
  (
    callback: WatchCallback,
  ): TerminatorGraph | Terminator;
  (
    callback: WatchCallback,
    tail: KeyGraph,
    nextTail?: KeyGraph
  ): TerminatorGraph | Terminator;
}

type KeyWatchMap = Map<WatchCallback, number>;
type WatcherMap = Map<Key, KeyWatchMap>;

let watchMap = new WeakMap<object, WatcherMap>();

export const customHandlerSymbols = new Set<symbol>();

export function notifyChange (
  obj: object,
  key: Key,
  meta?: unknown,
) {
  const watchers = watchMap.get(obj);

  if (!watchers) {
    return;
  }

  const keyWatches = watchers.get(key);
  if (!keyWatches) {
    return;
  }

  for (const callback of keyWatches.keys()) {
    callback(meta);
  }
}

export function notifyChanges (
  obj: object,
  keys: Key[],
  meta?: unknown,
) {
  const watchers = watchMap.get(obj);

  if (!watchers) {
    return;
  }

  const called = new Set<WatchCallback>();
  for (const key of keys) {
    const keyWatches = watchers.get(key);
    if (!keyWatches) {
      return;
    }

    for (const callback of keyWatches.keys()) {
      if (!called.has(callback)) {
        callback(meta);
        called.add(callback);
      }
    }
  }
}

/**
 * Used to watch `obj` for changes of `key`, when a change occurs the `callback` is run.
 * @private
 */
export function watchKey (
  obj: object,
  key: Key,
  callback: WatchCallback,
) {
  let watchers = watchMap.get(obj);
  if (!watchers) {
    watchers = new Map();
    watchMap.set(obj, watchers);
  }

  let keyWatches = watchers.get(key);
  if (!keyWatches) {
    keyWatches = new Map();
    watchers.set(key, keyWatches);
  }

  const callbackCount = (keyWatches.get(callback) || 0) + 1;
  keyWatches.set(callback, callbackCount);

  let canAttack = true;
  return () => {
    if (canAttack) {
      canAttack = false;
      attackWatchKey(obj, key, callback);
    }
  };
}

/**
 * Used to remove watches by decreasing the `callbackCount` for unique `obj, key, callback` sets
 * @private
 */
function attackWatchKey (
  obj: object,
  key: Key,
  callback: WatchCallback,
) {
  let watchers = watchMap.get(obj);
  if (!watchers) {
    return;
  }

  let keyWatches = watchers.get(key);
  if (!keyWatches) {
    return;
  }

  const callbackCount = keyWatches.get(callback);

  if (!callbackCount) {
    return;
  }

  if (callbackCount > 1) {
    keyWatches.set(callback, callbackCount - 1);
    return;
  }

  keyWatches.delete(callback);
  if (!keyWatches.size) {
    watchers.delete(key);
  }
  keyWatches = undefined;
  if (!watchers.size) {
    watchMap.delete(obj);
  }
  watchers = undefined;
}

export function terminateAllWatches () {
  watchMap = new WeakMap();
}

export function terminate (terminatorGraph: TerminatorGraph | Set<TerminatorGraph>) {
  for (const item of terminatorGraph) {
    if (Array.isArray(item) || item instanceof Set) {
      terminate(item);
      continue;
    }
    (item as Terminator)();
  }
}

export function watchGraph (
  callback: WatchCallback,
  obj: object,
  graph: KeyGraph,
  nextTail?: KeyGraph
): TerminatorGraph {
  const terminators: TerminatorGraph = [];

  let head: KeyBranch | Key;
  let tail: KeyLink | KeyBranch | Key | undefined;

  if (graph instanceof Set || typeof graph !== 'object') {
    head = graph;
  }
  else {
    head = graph.head;
    tail = graph.tail;
  }

  while (obj) {
    if (tail === undefined && nextTail) {
      tail = nextTail;
      nextTail = undefined;
    }

    // Branching graph path
    if (head instanceof Set) {
      const t: TerminatorGraph = [];
      for (const g of head) {
        //
        t.push(
          tail ?
            watchGraph(callback, obj, g, tail) :
            watchGraph(callback, obj, g)
        );
      }
      terminators.push(t);
      break;
    }
    if (typeof head === 'symbol' && customHandlerSymbols.has(head)) {
      if (head in obj) {
        // TODO: Improve typecheking if possible
        if (tail) {
          terminators.push((obj[head] as Handler)(callback, tail, nextTail));
        }
        else {
          terminators.push((obj[head] as Handler)(callback));
        }
      }

      break;
    }
    if (tail === undefined) {
      terminators.push(
        watchKey(obj, head, callback)
      );
      break;
    }

    const currentObj = obj;
    const nextObj: unknown = obj[head];
    const _head = head;
    const _tail = tail;
    const _tailGraph = nextTail;
    const terminatorsLength = terminators.length + 1;

    terminators.push([
      watchKey(obj, head, () => {
        const currentValue: unknown = currentObj[_head];

        if (currentValue !== nextObj) {
          while (terminators.length > terminatorsLength) {
            const t = terminators.pop()!;
            if (typeof t === 'function') {
              t();
            }
            else {
              terminate(t);
            }
          }

          if (typeof currentValue === 'object' && currentValue !== null) {
            terminators.push(...watchGraph(callback, currentValue, _tail, _tailGraph));
          }
        }
      }),
      watchKey(obj, head, callback)
    ]);

    if (typeof nextObj === 'object' && nextObj) {
      obj = nextObj;
    }
    else {
      break;
    }

    if (tail instanceof Set || typeof tail !== 'object') {
      head = tail;
      tail = undefined;
    }
    else {
      head = tail.head;
      tail = tail.tail;
    }
  }

  return terminators;
}

export function watch (
  obj: object,
  keyGraph: KeyGraph,
  callback: WatchCallback,
) {
  if (typeof keyGraph === 'string') {
    keyGraph = parseKeyGraph(keyGraph);
  }

  if (typeof keyGraph === 'string') {
    return watchKey(obj, keyGraph, callback);
  }

  const terminators = watchGraph(callback, obj, keyGraph);

  return () => terminate(terminators);
}
