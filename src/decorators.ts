import { notifyChange } from './internals';
import { parseKeyGraph } from './key-graph/parse';
import { KeyGraph, Key } from './key-graph/types';
import { watch } from './main';
import { registerEffect } from './agent';

export function watches (keyGraph: KeyGraph) {
  if (typeof keyGraph === 'string') {
    keyGraph = parseKeyGraph(keyGraph);
  }
  return function (target: object, key: Key, descriptor: PropertyDescriptor) {
    const method = descriptor.value;
    if (!method || typeof method !== 'function') {
      throw new Error('Only methods can be watched');
    }
    registerEffect(target, (agent) => watch(agent, keyGraph, method.bind(agent)));
    descriptor.configurable = false;
    descriptor.writable = false;
    return descriptor;
  };
}

export function computed (keyGraph: KeyGraph) {
  if (typeof keyGraph === 'string') {
    keyGraph = parseKeyGraph(keyGraph);
  }
  return (target: object, key: Key, descriptor: PropertyDescriptor) => {
    const getter = descriptor.get;
    if (!getter) {
      throw new Error('Computed descriptor must contain a getter');
    }
    const cachedValues = new WeakMap<object, unknown>();
    descriptor.get = function (this: object) {
      if (cachedValues.has(this)) {
        return cachedValues.get(this);
      }
      const result = getter.call(this);
      cachedValues.set(this, result);
      return result;
    };
    const setter = descriptor.set;
    if (setter) {
      descriptor.set = function (this: object, value: unknown) {
        cachedValues.set(this, setter.call(this, value));
      };
    }
    registerEffect(target, (agent) => watch(agent, keyGraph, () => {
      cachedValues.delete(agent);
      notifyChange(agent, key, undefined);
    }));
    return descriptor;
  };
}
