import { customHandlerSymbols } from '../internals';
import { symbolMap } from '../key-graph/parse';

export const eachSymbol = Symbol('@each');

symbolMap.set('@each', eachSymbol);
customHandlerSymbols.add(eachSymbol);