import { symbolMap } from '../key-graph/parse';
import { customHandlerSymbols } from '../internals';

export const eachKeySymbol = Symbol('@eachKey');

symbolMap.set('@eachKey', eachKeySymbol);
customHandlerSymbols.add(eachKeySymbol);