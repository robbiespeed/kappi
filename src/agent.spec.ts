import { expect } from 'chai';
import { registerEffect, startAgent, stopAgent } from './agent';
import { watch } from './internals';
import { set } from './main';

const wait = (ms: number) => {
  return new Promise<void>((resolve) => {
    setTimeout(resolve, ms);
  });
};

describe('Ticker Agent', () => {
  const makeTicker = () => {
    const ticker = { count: 0 };

    let cancelled = false;

    const tick = () => {
      if (cancelled) {
        return;
      }
      ticker.count++;

      setTimeout(tick, 1);
    };

    registerEffect(ticker, () => {
      tick();
      return () => {
        cancelled = true;
      };
    });

    return ticker;
  };

  it('should tick then not tick', async () => {
    const ticker = makeTicker();

    expect(ticker.count).to.equal(0);

    startAgent(ticker);
    await wait(10);

    const savedCount = ticker.count;
    expect(savedCount).to.be.greaterThan(1);

    stopAgent(ticker);
    await wait(10);

    expect(ticker.count).to.equal(savedCount);
  });
});

describe('Watched Agent', () => {
  interface Watched {
    x: number;
    y: number;
    xy: number;
  }

  const xyEffect = (agent: Watched) => {
    return watch(agent, 'x,y', () => {
      set(agent, 'xy', agent.x * agent.y);
    });
  };

  const makeWatched = () => {
    const watched: Watched = {
      x: 1,
      y: 1,
      xy: 1,
    };

    registerEffect(watched, xyEffect);

    return watched;
  };

  it('should update xy when x or y changes and agent is active', () => {
    const watched = makeWatched();

    startAgent(watched);
    expect(watched.xy).to.equal(1);

    set(watched, 'x', 2);
    expect(watched.xy).to.equal(2);

    set(watched, 'y', 2);
    expect(watched.xy).to.equal(4);
    stopAgent(watched);

    set(watched, 'y', 6);
    expect(watched.xy).to.equal(4);
  });
});
