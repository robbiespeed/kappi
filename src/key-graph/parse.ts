import { Key, KeyBranch, KeyLink, KeyGraph } from './types';

const branchStartMarker = '{';
const branchEndMarker = '}';
const pathMarker = '.';
const branchMarker = ',';

const keyMatcher = /^[^,\.\}]+/;

enum ItemType {
  PropKey,
  ParentKey,
  Branch,
  Path,
  End
}

interface ParsedItem {
  rest: string;
  item?: unknown;
}

interface ParsedPropKey extends ParsedItem {
  type: ItemType.PropKey;
  item: Key;
}

interface ParsedParentKey extends ParsedItem {
  type: ItemType.ParentKey;
  item?: undefined;
}

interface ParsedBranch extends ParsedItem {
  type: ItemType.Branch;
  item: KeyBranch;
}

interface ParsedPath extends ParsedItem {
  type: ItemType.Path;
  item: KeyLink;
}

interface ParsedEnd extends ParsedItem {
  type: ItemType.End;
  item?: undefined;
}

export const symbolMap = new Map<string, symbol>();

/**
 * Parse a graph string to find the first key or token,
 * and returns the key (if found) as well as the rest of the graph string
 * @param graphString A string representing a expandable path
 */
function parseKeyOrToken (graphString: string): { key?: Key; rest: string } {
  const keyMatch = keyMatcher.exec(graphString);
  let key = (keyMatch && keyMatch[0]) || undefined;

  let rest: string;

  if (key) {
    rest = graphString.slice(key.length);
    key = key.trim();

    if (!key) {
      throw new Error(`Cannot parse blank key at ${keyMatch!.index}`);
    }

    const token = symbolMap.get(key);

    return {
      key: token || key,
      rest
    };
  }

  return {
    rest: graphString,
  };
}

/**
 * Prefix a string to the head of the path
 * @param prefix The string used to add to the start of the heads key(s) or paths(s)
 * @param path The path recieving the prefix
 */
function prefixPath (prefix: string, path: KeyLink): KeyLink {
  const { head, tail } = path;
  if (typeof head === 'symbol') {
    throw new TypeError('Symbol cannot be prefixed');
  }
  return {
    head: typeof head === 'string' ?
      prefix + head :
      prefixHeadBranch(prefix, head),
    tail,
  };
}

/**
 * Prefix a string to a head branch
 * @param prefix The string used to add to the start of the heads key(s) or paths(s)
 * @param headBranch The path recieving the prefix
 */
function prefixHeadBranch (prefix: string, headBranch: KeyBranch): KeyBranch {
  const prefixedHead: KeyBranch = new Set();
  for (const key of headBranch) {
    if (typeof key === 'symbol') {
      throw new TypeError('Symbol cannot be prefixed');
    }
    prefixedHead.add(key + prefix);
  }

  return prefixedHead;
}

/**
 * Prefix a string to the paths of the branch
 * @param prefix The string used to add to the start of the branches key(s) or paths(s)
 * @param branch The branch recieving the prefix
 */
function prefixBranch (prefix: string, branch: KeyBranch): KeyBranch {
  const prefixedBranch: KeyBranch = new Set();
  for (const item of branch) {
    if (typeof item === 'symbol') {
      throw new TypeError('Symbol cannot be prefixed');
    }
    prefixedBranch.add(typeof item === 'string' ?
      item + prefix :
      prefixPath(prefix, item)
    );
  }
  return prefixedBranch;
}

function parseKeyOrPath (graphString: string): (
  ParsedPropKey | ParsedParentKey | ParsedBranch | ParsedPath
) {
  const { key, rest } = parseKeyOrToken(graphString);
  const nextChar = rest.charAt(0);

  if (key === undefined) {
    if (nextChar === branchMarker) {
      return {
        type: ItemType.ParentKey,
        rest
      };
    }
    else {
      throw new Error(`Expected graph string "${graphString}" to start with a key`);
    }
  }

  if (nextChar === pathMarker) {
    const {
      item,
      rest: _rest,
    } = parseItem(rest.slice(1));

    if (!item) {
      throw new Error(`Graph string "${graphString}" cannot end with a path marker`);
    }

    const path: KeyLink = {
      head: key,
      tail: item,
    };

    return {
      type: ItemType.Path,
      item: path,
      rest: _rest,
    };
  }
  else if (nextChar === branchStartMarker) {
    if (typeof key === 'symbol') {
      throw new TypeError('Symbol cannot be a prefix');
    }
    const parsedItem = parseBranch(rest.slice(1));
    const { type } = parsedItem;

    return {
      type,
      item: prefixBranch(key, parsedItem.item as KeyBranch),
      rest: parsedItem.rest,
    };
    // if (type === ItemType.Branch) {
    // }
    // else if (type === ItemType.PropKey) {
    //   if (typeof parsedItem.item !== 'string') {
    //     throw new TypeError('Only strings can be prefixed');
    //   }
    //   return {
    //     type,
    //     item: key + parsedItem.item,
    //     rest: parsedItem.rest,
    //   };
    // }
    // else if (type === ItemType.Path) {
    //   return {
    //     type,
    //     item: prefixPath(key, parsedItem.item as KeyPath),
    //     rest: parsedItem.rest,
    //   };
    // }
  }

  return {
    type: ItemType.PropKey,
    item: key,
    rest,
  };
}

function parseBranch (graphString: string): ParsedBranch {
  const branch: KeyBranch = new Set();
  let rest = graphString;
  let nextChar: string | undefined;

  while (rest.length) {
    const parsed = parseItem(rest);
    rest = parsed.rest;
    const { item, type } = parsed;

    if (type === ItemType.Branch) {
      for (const _item of item as KeyBranch) {
        branch.add(_item);
      }
    }
    else if (type === ItemType.End) {
      throw new Error('Unexpected end of branch');
    }
    else if (item) {
      branch.add(item as KeyLink | string);
    }

    nextChar = rest.charAt(0);

    if (nextChar === branchMarker) {
      rest = rest.slice(1);
    }
    else if (nextChar === branchEndMarker) {
      rest = rest.slice(1);
      break;
    }
  }

  return {
    type: ItemType.Branch,
    item: branch,
    rest,
  };
}

function parseItem (graphString: string): (
  ParsedPropKey |
  ParsedParentKey |
  ParsedBranch |
  ParsedPath |
  ParsedEnd
) {
  const nextChar = graphString.charAt(0);

  if (!nextChar) {
    return {
      type: ItemType.End,
      rest: graphString,
    };
  }

  if (nextChar === branchStartMarker) {
    const parsed = parseBranch(graphString.slice(1));
    const { rest } = parsed;

    if (parsed.item.size === 1) {
      // TODO: Don't error instead break open branch and expand to contents
      throw new Error('Branch should have more than one path');
    }

    if (rest.charAt(0) === pathMarker) {
      const _parsed = parseItem(rest.slice(1));

      if (_parsed.item === undefined) {
        throw new Error('Unexpected end of path');
      }

      return {
        type: ItemType.Path,
        rest: _parsed.rest,
        item: {
          head: parsed.item,
          tail: _parsed.item
        }
      };
    }

    return parsed;
  }

  return parseKeyOrPath(graphString);
}

export function parseKeyGraph (graphString: string): KeyGraph {
  const keyGraph = parseBranch(graphString).item;

  switch (keyGraph.size) {
    case 0:
      throw new Error('Could not parse graph string');
    case 1:
      const [ item ] = keyGraph;
      return item;
  }

  return keyGraph;
}
