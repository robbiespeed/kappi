import { expect } from 'chai';

import parseKeyGraph, { symbolMap } from './parse';

describe('parseKeyGraph(keyTreeString)', () => {
  it('should parse single key', () => {
    const graph = parseKeyGraph('keyName');

    expect(graph).to.equal('keyName');
  });
  it('should expand commas as branches', () => {
    const graph = parseKeyGraph('keyA,keyB,keyC');

    expect(graph).to.deep.equal(new Set(['keyA', 'keyB', 'keyC']));
  });
  it('should expand dots as paths', () => {
    const graph = parseKeyGraph('keyA.keyB');

    expect(graph).to.deep.equal({
      head: 'keyA',
      tail: 'keyB',
    });
  });
  it('should allow whitespace between keys', () => {
    const graph = parseKeyGraph('keyA , keyB . keyC');

    expect(graph).to.deep.equal(new Set([
      'keyA',
      {
        head: 'keyB',
        tail: 'keyC',
      },
    ]));
  });
  it('should expand long paths', () => {
    const graph = parseKeyGraph('keyA.keyB.keyC');

    expect(graph).to.deep.equal({
      head: 'keyA',
      tail: {
        head: 'keyB',
        tail: 'keyC',
      },
    });
  });
  it('should expand braces as nested branches on path', () => {
    const graph = parseKeyGraph('keyA.{keyB,keyC}');

    expect(graph).to.deep.equal({
      head: 'keyA',
      tail: new Set([
        'keyB',
        'keyC',
      ]),
    });
  });
  it('should expand root braces as begining branches on path', () => {
    const graph = parseKeyGraph('{keyA,keyB}.keyC');

    expect(graph).to.deep.equal({
      head: new Set(['keyA', 'keyB']),
      tail: 'keyC',
    });
  });
  it('should handle {a,b}.{c,d}.{e,f}', () => {
    const graph = parseKeyGraph('{a,b}.{c,d}.{e,f}');

    expect(graph).to.deep.equal({
      head: new Set(['a', 'b']),
      tail: {
        head: new Set(['c', 'd']),
        tail : new Set(['e', 'f'])
      },
    });
  });
  it('should handle a,b.{c,d}.{e.f,g.{h,i},j}.k,l', () => {
    const graph = parseKeyGraph('a,b.{c,d}.{e.f,g.{h,i},j}.k,l');

    expect(graph).to.deep.equal(new Set([
      'a',
      {
        head: 'b',
        tail: {
          head: new Set(['c', 'd']),
          tail: {
            head: new Set([
              {
                head: 'e',
                tail: 'f'
              },
              {
                head: 'g',
                tail: new Set([
                  'h',
                  'i',
                ])
              },
              'j',
            ]),
            tail: 'k'
          }
        },
      },
      'l',
    ]));
  });
  it('should optimize shallow non pathed branch to top level branch', () => {
    const graph = parseKeyGraph('{keyA,keyB,keyC}');

    expect(graph).to.deep.equal(new Set(['keyA', 'keyB', 'keyC']));
  });
  it('should parse multiple nested braces and paths', () => {
    let graph = parseKeyGraph('keyA.{keyB.{a,b},keyC}.c');

    expect(graph).to.deep.equal({
      head: 'keyA',
      tail: {
        head: new Set([
          {
            head: 'keyB',
            tail: new Set(['a', 'b']),
          },
          'keyC',
        ]),
        tail: 'c',
      },
    });

    graph = parseKeyGraph('keyA.{keyB.{a,b},keyC},c');

    expect(graph).to.deep.equal(new Set([
      {
        head: 'keyA',
        tail: new Set([
          {
            head: 'keyB',
            tail: new Set(['a', 'b']),
          },
          'keyC',
        ]),
      },
      'c',
    ]));

    graph = parseKeyGraph('keyA.{keyB.{a,b},keyC.a},c');

    expect(graph).to.deep.equal(new Set([
      {
        head: 'keyA',
        tail: new Set([
          {
            head: 'keyB',
            tail: new Set(['a', 'b'])
          },
          {
            head: 'keyC',
            tail: 'a',
          },
        ]),
      },
      'c',
    ]));

    graph = parseKeyGraph('keyA.{keyB.{a,b},keyC.a}.c');

    expect(graph).to.deep.equal({
      head: 'keyA',
      tail: {
        head: new Set([
          {
            head: 'keyB',
            tail: new Set(['a', 'b'])
          },
          {
            head: 'keyC',
            tail: 'a',
          },
        ]),
        tail: 'c',
      },
    });
  });
  context('When @special token is defined', () => {
    before(() => {
      symbolMap.set('@special', Symbol('@special token'));
    });
    after(() => {
      symbolMap.delete('@special');
    });
    it('should parse @special as a token', () => {
      const graph = parseKeyGraph('@special');
      expect(graph).to.equal(symbolMap.get('@special'));
    });
  });
});