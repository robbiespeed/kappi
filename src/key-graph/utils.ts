import { KeyBranch, KeyGraph, KeyLink, Key } from './types';

function _traverseGraph (
  callback: (o: object, k: Key, depth: number) => boolean,
  startDepth = 0,
  obj: object,
  graph: KeyGraph,
  ...tailGraphs: KeyGraph[]
) {
  let depth = startDepth;
  let head: KeyBranch | Key;
  let tail: KeyLink | KeyBranch | Key | undefined;

  if (graph instanceof Set || typeof graph !== 'object') {
    head = graph;
    tail = tailGraphs.pop();
  }
  else {
    head = graph.head;
    tail = graph.tail;
  }

  while (obj) {
    if (tail === undefined && tailGraphs.length) {
      tail = tailGraphs.pop();
    }

    if (head instanceof Set) {
      for (const g of head) {
        if (tail) {
          _traverseGraph(callback, depth, obj, g, ...tailGraphs, tail);
        }
        else {
          _traverseGraph(callback, depth, obj, g, ...tailGraphs);
        }
      }
      break;
    }

    const shouldContinue = callback(obj, head, depth);
    if (!shouldContinue) {
      break;
    }

    const next: unknown = obj[head];
    if (typeof next === 'object' && next && tail !== undefined) {
      depth++;

      obj = next;

      if (tail instanceof Set || typeof tail !== 'object') {
        head = tail;
        tail = undefined;
      }
      else {
        head = tail.head;
        tail = tail.tail;
      }
    }
    else {
      break;
    }
  }
}

export function traverseGraph (
  callback: (o: object, k: Key, depth: number) => void,
  obj: object,
  graph: KeyGraph,
  ...tailGraphs: KeyGraph[]
) {
  _traverseGraph((...args) => (callback(...args), true), 0, obj, graph, ...tailGraphs);
}

interface GraphObject {
  [key: string]: GraphObject | undefined;
}

export function objectFromGraph (graph: KeyGraph) {
  const obj: GraphObject = {};

  traverseGraph((_obj: any, key) => {
    _obj[key] = {};
  }, obj, graph);

  return obj;
}

export function traverseGraphPartialDepth (
  callback: (o: object, k: Key, depth: number) => boolean,
  obj: object,
  graph: KeyGraph,
  ...tailGraphs: KeyGraph[]
) {
  _traverseGraph(callback, 0, obj, graph, ...tailGraphs);
}
