import { expect } from 'chai';

import { set, watch } from './main';
import parseKeyGraph from './key-graph/parse';
import { traverseGraph, objectFromGraph } from './key-graph/utils';

interface TestObj {
  [key: string]: TestObj | number;
}

describe('set(obj, key, value)', () => {
  it('should assign the new value', () => {
    const obj = { a: 0 };

    set(obj, 'a', 1);

    expect(obj.a).to.equal(1);
  });
});

describe('watch(obj, keyGraph, callback)', () => {
  context(`when a single property is watched ( keyGraph = 'a' )`, () => {
    it(`should run callback after property 'a' is set`, () => {
      const obj: TestObj = {};
      let watchCount = 0;
      watch(obj, 'a', () => watchCount++);
      set(obj, 'a', 1);
      expect(watchCount).to.equal(1);
    });
    it('should not run callback after watcher has been terminated', () => {
      const obj: TestObj = {};
      let watchCount = 0;
      const watchTerminator = watch(obj, 'a', () => watchCount++);
      set(obj, 'a', 1);
      watchTerminator();
      set(obj, 'a', 1);
      expect(watchCount).to.equal(1);
    });
  });

  context(`when mutliple shallow properties are watched ( keyGraph = 'a,b' )`, () => {
    const graph = parseKeyGraph('a,b') as Set<string>;

    graph.forEach((key) => {
      it(`should run callback after property '${key}' is set`, () => {
        const obj: TestObj = {};
        let watchCount = 0;
        watch(obj, 'a,b', () => watchCount++);

        set(obj, key, 1);
        expect(watchCount).to.equal(1);
      });
    });
    it('should not run callback after watcher has been terminated', () => {
      const obj: TestObj = {};
      let watchCount = 0;
      const watchTerminator = watch(obj, 'a,b', () => watchCount++);
      watchTerminator();
      graph.forEach((key: string) => {
        set(obj, key, 1);
      });
      expect(watchCount).to.equal(0);
    });
  });

  context(`when a multi-branching keygraph is used ( keygraph = '{a,b}.c.{d,e}.f')`, () => {
    const cMaker = () => ({
      c: {
        d: { f: 1 },
        e: { f: 1 },
      },
    });
    const objMaker = () => ({
      a : cMaker(),
      b : cMaker(),
    });

    it('should respect the later part of the chain when a base property is set', () => {
      const obj = objMaker();
      let watchCount = 0;

      watch(obj, '{a,b}.c.{d,e}.f', () => watchCount++);

      set(obj, 'a', cMaker());
      expect(watchCount).to.equal(1);
      set(obj.a.c.d, 'f', 2);
      expect(watchCount).to.equal(2);
    });
  });

  const nestedCases = [
    {
      discription: 'when a chain of nested properties are watched',
      graphString: 'a.b.c',
    },
    {
      discription: 'when a chain of nested properties are watched',
      graphString: 'a.a.a',
    },
    {
      discription: 'when multiple nested properties are watched',
      graphString: 'a.b.c,d.e.f',
    },
    {
      discription: 'when multiple nested properties are watched using a branching KeyGraph',
      graphString: 'a,b.{c,d}.{e.f,g.{h,i},j}.k,l',
    },
  ];

  nestedCases.forEach(({ discription, graphString }) => {
    const keyGraph = parseKeyGraph(graphString);
    const json = JSON.stringify(objectFromGraph(keyGraph));
    const objFactory = () => JSON.parse(json);

    context(`${discription} ( keyGraph = '${graphString}' )`, () => {
      it('should run callback after any watched property is set', () => {
        const obj: TestObj = objFactory();
        let desiredTotalWatchCount = 0;
        traverseGraph(() => {
          desiredTotalWatchCount++;
        }, obj, keyGraph);

        let watchCount = 0;
        watch(obj, graphString, () => watchCount++);

        let travelCount = 0;
        traverseGraph((_obj, key) => {
          travelCount++;
          set(_obj as any, key, {});
          expect(watchCount).to.equal(travelCount, `key '${key as string}'`);
        }, obj, keyGraph);
        expect(watchCount).to.equal(desiredTotalWatchCount);
      });

      it(`should handle setting all watched properties to the parent`, () => {
        const obj: TestObj = objFactory();
        let watchCount = 0;
        watch(obj, graphString, () => watchCount++);

        const keys = new Set<string>();

        let travelCount = 0;
        traverseGraph((_obj: any, key) => {
          keys.add(key as string);
          if (_obj[key] !== _obj) {
            // If the parent is already set to itself the set wont trigger any watchers
            travelCount++;
          }
          set(_obj, key as string, _obj as TestObj);
          expect(watchCount).to.equal(travelCount, `key '${key as string}'`);
        }, obj, keyGraph);

        expect(obj).to.contain.all.keys(...keys);
      });

      it('should not run callback after watcher has been terminated', () => {
        const obj: TestObj = objFactory();
        let watchCount = 0;
        const watchTerminator = watch(obj, graphString, () => watchCount++);

        watchTerminator();
        traverseGraph((_obj: any, key) => {
          set(_obj, key, {});
        }, obj, keyGraph);
        expect(watchCount).to.equal(0);
      });

    });
  });

  context('when a self referencing obj is watched', () => {
    it('should only fire the watch once', () => {
      const obj: TestObj = {};
      obj.a = obj;
      let watchCount = 0;
      watch(obj, 'a.a.a', () => {
        watchCount++;
      });
      set(obj, 'a', 1);
      expect(watchCount).to.equal(1);
      set(obj, 'a', obj);
      expect(watchCount).to.equal(2);
    });
  });

  it('should not run watcher when old deep property is set', () => {
    const objA: TestObj = {};
    const obj = { a: objA };
    let watchCount = 0;
    watch(obj, 'a.b.c', () => {
      watchCount++;
    });

    set(obj, 'a', {});
    set(objA, 'b', 1);
    expect(watchCount).to.equal(1);
  });
  it('should handle circular references', () => {
    const a: any = {};
    const b = { a };
    a.b = b;
    const obj = { a, b };
    let watchCount = 0;
    const watchTerminator = watch(obj, 'a.b.a.b', () => {
      watchCount++;
    });

    const b2 = { a };
    set(a, 'b', b2);
    expect(watchCount).to.equal(1);
    set(b, 'a', 1);
    expect(watchCount).to.equal(1);
    set(b2, 'a', 1);
    expect(watchCount).to.equal(2);

    watchTerminator();
    set(b2, 'a', 2);
    expect(watchCount).to.equal(2);
    set(a, 'b', 2);
    expect(watchCount).to.equal(2);
  });
  it('should only run callback once even if object is referenced more than once', () => {
    const vObj = {
      v: 0
    };
    const obj = {
      a: vObj,
      b: vObj,
    };
    let watchCount = 0;
    watch(obj, '{a,b}.v', () => {
      watchCount++;
    });

    set(vObj, 'v', 1);
    expect(watchCount).to.equal(1);
  });
});
