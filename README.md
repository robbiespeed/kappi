# Kappi
Reactive state managment, built with TypeScript.

Kappi aims to be:
- minimal
- easy to understand; both in use and internals

## Build
`yarn run build`

## Test
`yarn test`
