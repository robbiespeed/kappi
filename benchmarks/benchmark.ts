import { Suite } from 'benchmark';
import parseKeyGraph from '../src/key-graph/parse';
import { watch, set } from '../src/main';

const suite = new Suite();

suite.add('parse "a"', () => {
  parseKeyGraph('a');
});
suite.add('parse "a,b"', () => {
  parseKeyGraph('a,b');
});
suite.add('parse "a,b.c.{d,e},f"', () => {
  parseKeyGraph('a,b.c.{d,e},f');
});

let obj: { [p: string]: any } = { a: 'string' };
suite.on('cycle', () => {
  obj = { a: 'string' };
});
suite.add('watch "a"', () => {
  const t = watch(obj, 'a', () => {});
  set(obj, 'a', 1);
  t();
});
suite.add('watch "a.b.{c,d,e}.f"', () => {
  const t = watch(obj, 'a.b.{c,d,e}.f', () => {});
  set(obj, 'a', 1);
  set(obj, 'a', { b: { c: { f: 3, }, d: { f: 'string' } }});
  set(obj.a.b, 'e', {});
  set(obj, 'a', {});
  t();
});

suite.on('error', (error: any) => {
  console.error(error);
});
suite.on('cycle', (event: any) => {
  console.log(String(event.target));
});

suite.run();
